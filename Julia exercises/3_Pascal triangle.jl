#= Implement a function pascal_triangle(n) which prints the first n rows of Pascal’s triangle. For example, pascal_triangle(5) should print
    1
   1 1
  1 2 1
 1 3 3 1
1 4 6 4 1
At the start, you might consider ignoring the proper layout of the triangle. =#

function pascal_triangle(n)
    rows = [[1]]
    for i in 2:n
        push!(rows, [1; [rows[i-1][j-1] + rows[i-1][j] for j in 2:i-1]; 1])
    end
    return rows
end

pascal_triangle(5)
