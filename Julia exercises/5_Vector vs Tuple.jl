## 1. Observe the difference in timings for the following two ways of calculating the millionth Fibonacci number (modulo 2^64).

F(a::Tuple{Int, Int}) = (a[2], a[1]+a[2])
F(a::Vector{Int})     = [a[2], a[1]+a[2]]

function G(a, n)
  for i in 1:n
    a = F(a)
  end
  return a
end

@time G((0,1), 10^6)    # 0.000461 seconds
@time G([0,1], 10^6)    # 0.048970 seconds


## 2. Write a function `H` that accepts an input `n::Int` and returns a `Tuple{Bool, Int}` where the first return indicates whether `n` is a square and the second return is the positive square root if it exists or zero otherwise.

function H(n::Int)
    if n >= 0
        rt = floor(Int, sqrt(n))
        if n == rt*rt
            return (true, rt)
        end
    end
    return (false, 0)
end

# You should have
@assert H(0) == (true, 0)
@assert H(1) == (true, 1)
@assert H(2) == (false, 0)
@assert H(3) == (false, 0)
@assert H(4) == (true, 2)
@assert H(-1) == (false, 0)

# Try to change `H` to return a vector and observe what happens to the type of the return.

function H2(n::Int)
    if n >= 0
        rt = floor(Int, sqrt(n))
        if n == rt*rt
            return [true, rt]
        end
    end
    return [false, 0]
end

@show H2(0) # [1, 0] instead of (true, 0)
@show H2(1)
@show H2(2)
@show H2(3)
@show H2(4)
# The bools get converted to Int
