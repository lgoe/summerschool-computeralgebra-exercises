# Consider the following type, which defines a permutation:
struct Permutation
    images::Vector{Int}
end
# Thus the permutation with `images` set to `[1, 2, 3]` is the identity on 3 letters and `[2, 1, 3] `is the transposition (1,2) on 3 letters.

## 1. Fill in the stub below to write a function for multiplying two permutations. Permutations are applied from the right, so that (1,2)(2,3) = (1,3,2). Test your function by computing `Permutation([2, 1, 3]) * Permutation([1, 3, 2])`.

function *(x::Permutation, y::Permutation)
    @assert length(x.images) == length(y.images)
    n = length(x.images)
    return Permutation([y.images[x.images[i]] for i in 1:n])
end

@show Permutation([2, 1, 3]) * Permutation([1, 3, 2])


## 2. Write a function `apply` that applies a `Permutation` to an `Int`. Make sure that the function has the right signature.

function apply(p::Permutation, x::Int)
    @assert 1 <= x <= length(p.images)
    return p.images[x]
end

p = Permutation([2, 3, 4, 5, 1])
@show apply(p, 1) == 2
@show apply(p, 5) == 1


## 3. Write another `apply` method that entry-wise applies a permutation to a tuple.

function apply(p::Permutation, xs::Tuple)
    return map(x -> apply(p, x), xs)
end

p = Permutation([2, 3, 4, 5, 1])
@show apply(p, (1, 5)) == (2, 1)


## 4. Write an `apply!` function that modifies a given `Vector` of the right length by permuting its entries according to `x[i] = x[p(i)]`, and returns it.

function apply!(p::Permutation, x::Vector)
    @assert length(x) == length(p.images)
    n = length(x)
    return x[:] = [x[p.images[i]] for i in 1:n]
end

p = Permutation([2, 3, 4, 5, 1])
x = ["a", "b", "c", "d", "e"]
@show apply!(p, x)
@show x == ["b", "c", "d", "e", "a"]


## 5. Derive a method `apply(p::Permutation, x::Vector)` which does the same as `apply!` without changing the input.

function apply(p::Permutation, x::Vector)
    return apply!(p, copy(x))
end

p = Permutation([2, 3, 4, 5, 1])
x = ["a", "b", "c", "d", "e"]
@show apply(p, x) == ["b", "c", "d", "e", "a"]
@show x == ["a", "b", "c", "d", "e"]
