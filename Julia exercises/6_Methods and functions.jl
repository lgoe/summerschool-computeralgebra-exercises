## 1. What arguments does `pushfirst!` expect?

methods(pushfirst!)


## 2. Without `using Oscar`, list all functions that accept a `Vector` argument.

methodswith(Vector)


## 3. After `using Oscar`, list all functions that accept an `FmpzPolyRing`.

using Oscar
methodswith(FmpzPolyRing)


## 4. List all functions that accept both an `fmpz_mat` and an `fmpz`.

intersect(methodswith(fmpz_mat), methodswith(fmpz))


## 5. Find the source code for the method computing the power `fmpz(2)^10`.

@which fmpz(2)^10
@less fmpz(2)^10
@edit fmpz(2)^10
