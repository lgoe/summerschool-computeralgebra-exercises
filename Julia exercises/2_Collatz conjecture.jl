## 1. Implement a function f(n::Int) which returns n/2 if n is even and 3n+1 if n is odd.

function f(n::Int)
    return iseven(n) ? n ÷ 2 : 3n + 1
end

## 2. Implement a function g(n::Int) which determines the smallest k with f^k(n) = 1.

function _g(f, n::Int)
    k = 0
    while n != 1
        n = f(n)
        k += 1
    end
    return k
end

function g(n::Int)
    return _g(f, n)
end

@show g.(1:10)

## 3. Write a program which finds two numbers >100 for which g returns the same value.

function h()
    b = 101

    while true
        for a = 101:b
            if a != b && g(a) == g(b)
                return a, b
            end
        end
        b += 1
    end
end

@show h()
