## Write a function `cached(f)` that takes a function/callable object `f` and returns a new function which returns the same values as `f` but caches the return value for any input value. 

function cached(f)
    cache = Dict()
    return function (args...)
        if !haskey(cache, args)
            cache[args] = f(args...)
        end 
        return cache[args]
    end
end

# Test it with various functions:
g = cached(+)
@show g(1, 1) == 1 + 1

#To check the caching, try the following function:
h = function (s)
    sleep(1)
    return s
end

g = cached(h)
@time g(1) # this should take 1 second
@time g(1) # this should take almost no time
@time g(2) # this should take 1 second
