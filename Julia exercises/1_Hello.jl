## 1. Implement a hello world function hello_world() which prints “hello world”.

function hello_world()
    println("hello world")
end

hello_world()


## 2. Implement a hello function hello(name::String) which prints “hello x”, where x is the first argument.
function hello(name::String)
    println("hello $name")
end

hello("lars")
