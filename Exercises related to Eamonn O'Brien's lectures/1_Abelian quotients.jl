using Oscar
using Summerschool21Exercises

## 1. Define the group `H = ⟨x,y | x^2*y^2=1, x^-1*y^3*x^4*y=1⟩`.

G = free_group("x","y")
x, y = gens(G)
# H, _ = quo(G, [x^2*y^2, x^-1*y^3*x^4*y])
H, _ = quo(G, [x^3, y^2, x^-1*y^-1*x*y])
x, y = gens(H)


## 2. Write a Julia function that computes the isomorphism type of the abelian quotient `G/G′` of a given `FPGroup` `G` via the Smith normal form of the matrix of abelianized relators. Apply it to the group `H` defined above.

function group2matrix(G::FPGroup)
    rel = relators(G)
    return matrix(ZZ, map((a -> Vector{fmpz}(a, ngens(G))) ∘ abelianized ∘ SyllableVector{fmpz}, rel))
end

function abquo_isotype(G::FPGroup)
    S = snf(group2matrix(G))
    iso = zeros(fmpz, ncols(S))
    for i in 1:min(ncols(S), nrows(S))
        iso[i] = S[i,i]
    end
    return iso
end

abquo_isotype(H)


## 3. Test the function by comparing the results with the GAP function available in Oscar.

GAP.Globals.AbelianInvariants(H.X)
# ignores the 1 which is ok since ZZ_1 is the trivial group


## 4. In order to write down a group epimorphism from `G` to `G/G′` that maps the generators of `G` to the corresponding integer vectors, one needs not only the Smith normal form but also a transformation matrix. Write a Julia function that computes the epimorphism.
H_abl = abelian_group(PcGroup, Vector{Int64}(abquo_isotype(H)))
H_gens = gens(H_abl)
S, T, U = snf_with_transform(group2matrix(H)) # S = TMU
# T replaces the relations by other equivalent relations
# the rows of U give the original generators in terms of new generators
exps = Array(U)
gens_with_exponents(gens::Vector{<: GroupElem}, exp::Vector{fmpz}) = prod(gens[i]^exp[i] for i in 1:length(gens))
f = hom(H, H_abl, [x, y], [gens_with_exponents(H_gens, exps[1,:]), gens_with_exponents(H_gens, exps[2,:])])


## 5. The entries in the transformation matrices that occur in the computation of the Smith normal form can get quite large. Make some experiments with random integer matrices whose entries have small absolute value. Consider also `n` by `n` diagonal matrices with diagonal entries `1,2,…,n`.
n = 20
M = matrix(ZZ, zeros(fmpz, n, n))
for i in 1:n
    M[i,i] = i
end
T, S, U = snf_with_transform(M)
maximum(abs, U)
