using Oscar

## 1. Define the number field `K = QQ(√2)`.

Qx, x = QQ["x"]
f = x^2 - 2
K, _ = number_field(f)

# Alternative: K, _ = radical_extension(2, QQ(2))


## 2. Find a normal extension `L/K` such that `Gal(L/K) ≅ C_2 × C_2`.
Ky, y = K["y"]
L, _ = number_field([y^2 - 3, y^2 - 5])
grp, _ = automorphism_group(L)
#isisomorphic(grp, direct_product(cyclic_group(2), cyclic_group(2)))
# the above does not work right now. workaround:
describe(small_group(find_small_group(grp)[1]...))


## 3. Find all normal extension `L/K` as in part 2 such that `L/QQ` is normal and the absolute discriminant of `L` is bounded by `10^10`.

exts = abelian_normal_extensions(K, [2, 2], ZZ(10)^10)


## 4. Determine a defining polynomial for one of the fields `L` found in part 3. What is `Gal(L/QQ)`?
L, _ = absolute_simple_field(number_field(exts[1]))
grp, _ = automorphism_group(L, QQ)
describe(small_group(find_small_group(grp)[1]...))
grp, _ = galois_group(L)
describe(grp)
