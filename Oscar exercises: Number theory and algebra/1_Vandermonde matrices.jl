using Oscar

## 1. Create the polynomial ring `R = QQ[x_1,...,x_5]`.

R, x = PolynomialRing(QQ, ["x_$i" for i in 1:5])


## 2. Create the Vandermonde matrix.

V = matrix(R, [x[i]^j for i in 1:5, j in 0:4])


## 3. Compute and factorize the determinant det(V).

d = det(V)
@show factor(d)

## 4. Pick 10 random elements `p ∈ QQ^5` and verify that `det(V)(p) = det(V(p))`.
for _ in 1:10
    p = [rand(QQ, -10:10) for _ in 1:5]
    @show evaluate(d, p) == det(map_entries(f -> evaluate(f, p), V))
end
