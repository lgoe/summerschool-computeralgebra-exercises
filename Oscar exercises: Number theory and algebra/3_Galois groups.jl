using Oscar

## 1. Determine the Galois group of `f = x^3 + x + 1 ∈ ZZ[x]`

Zx, x = ZZ["x"]
f = x^3 + x + 1
K, _ = number_field(f)
gal_grp1, _ = galois_group(f)
galois_group(K)


## 2. Find a polynomial of degree `6` with the same Galois groups as `f`.
L, _ = normal_closure(K)
g = defining_polynomial(L)
gal_grp2, _ = galois_group(g)
galois_group(L)

isisomorphic(gal_grp1, gal_grp2)


## 3. Determine the Galois groups of 1000 random monic, irreducible polynomials in `ZZ[x]` of degree 3 and coefficients bounded in absolute value by 10. What is the distribution?

Zx, x = ZZ["x"]
left = 1000
distribution = Dict{PermGroup, Int}()
while left > 0
    f = x^3 + rand(Zx, 2:2, -10:10)
    if isirreducible(f)
        grp, _ = galois_group(f)
        if !haskey(distribution, grp)
            distribution[grp] = 0
        end
        distribution[grp] += 1
        left -= 1
    end
end

@show distribution


## 4. For all transitive groups of order 4, find a monic irreducible polynomial of degree 4 of `ZZ[x]` with that Galois group (random monic polynomials with coefficients in absolute value bounded by 10 will do).

Zx, x = ZZ["x"]
left = fill(true, number_transitive_groups(4))
polys = Vector{elem_type(Zx)}(undef, number_transitive_groups(4))
grps = Vector{PermGroup}(undef, number_transitive_groups(4))
while any(left)
    f = x^4 + rand(Zx, 3:3, -10:10)
    if isirreducible(f)
        grp, _ = galois_group(f)
        id = transitive_group_identification(grp)
        if left[id]
            left[id] = false
            polys[id] = f
            grps[id] = grp
        end
    end
    println(sum(left))
end

polys
grps
