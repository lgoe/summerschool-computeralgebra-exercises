using Oscar
import AbstractAlgebra: Ring, RingElem


## The goal of this exercise is to implement `R × S`, the product of two commutative rings `R` and `S`.

mutable struct ProdRing{T <: Ring, U <: Ring} <: Ring
    first::T
    second::U
end

mutable struct ProdRingElem{V <: RingElem, W <: RingElem} <: RingElem
    first::V
    second::W
    parent::ProdRing{<: Ring, <: Ring}
end

## Implementation:

# Connect the types
function AbstractAlgebra.parent_type(::Type{ProdRingElem{V, W}}) where {V <: RingElem, W <: RingElem}
    return ProdRing{parent_type(V), parent_type(W)}
end

function AbstractAlgebra.elem_type(::Type{ProdRing{T, U}}) where {T <: Ring, U <: Ring}
    return ProdRingElem{elem_type(T), elem_type(U)}
end

################################################################################
# Basic functions
################################################################################

# Constructors
function product_ring(R :: T, S :: U) where {T <: Ring, U <: Ring}
    return ProdRing{T, U}(R, S)
end

function (P::ProdRing{T, U})() where {T <: Ring, U <: Ring}
    return ProdRingElem{elem_type(T), elem_type(U)}(P.first(), P.second(), P)
end

function (P::ProdRing{T, U})(x::ProdRingElem{V, W}) where {T <: Ring, U <: Ring, V <: RingElem, W <: RingElem}
    @assert parent(x) == P
    return x
end

function (P::ProdRing{T, U})(v::V, w::W) where {T <: Ring, U <: Ring, V <: RingElem, W <: RingElem}
    @assert parent(v) == P.first
    @assert parent(w) == P.second
    return ProdRingElem{V,W}(v, w, P)
end

function (P::ProdRing{T, U})(x::Int64) where {T <: Ring, U <: Ring}
    return ProdRingElem{elem_type(T), elem_type(U)}(P.first(x), P.second(x), P)
end

function (P::ProdRing{T, U})(x::Int64, y::Int64) where {T <: Ring, U <: Ring}
    return ProdRingElem{elem_type(T), elem_type(U)}(P.first(x), P.second(y), P)
end

# Parent
Base.parent(x::ProdRingElem) = x.parent

# Show
function Base.show(io::IO, P::ProdRing)
    print(IOContext(io, :compact => true), P.first)
    print(io, " × ")
    print(IOContext(io, :compact => true), P.second)
end

function Base.show(io::IO, x::ProdRingElem)
    print(io, (x.first, x.second))
end


# Hashing
Base.hash(x::ProdRingElem, h::UInt) = Base.hash(x.first, Base.hash(x.second, h))

# Random Element creation
AbstractAlgebra.rand(P::ProdRing) = P(rand(P.first), rand(P.second))

# Deepcopy. Avoid copying parents
function Base.deepcopy_internal(x::ProdRingElem, dict::IdDict)
    parent(x)(deepcopy(x.first), deepcopy(x.second))
end

################################################################################
# Arithmetic
################################################################################

function Base.:(==)(x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    return x.first == y.first && x.second == y.second
end

Base.zero(P::ProdRing) = P(zero(P.first), zero(P.second))
Base.one(P::ProdRing) = P(one(P.first), one(P.second))

Base.iszero(x::ProdRingElem) = return iszero(x.first) && iszero(x.second)
Base.isone(x::ProdRingElem) = return isone(x.first) && isone(x.second)


function Base.:(+)(x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    @assert parent(x) == parent(y)
    return parent(x)(x.first + y.first, x.second + y.second)
end

function Base.:(-)(x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    @assert parent(x) == parent(y)
    return parent(x)(x.first - y.first, x.second - y.second)
end

function Base.:(-)(x::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    return parent(x)(-x.first, -x.second)
end

function Base.:(*)(x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    @assert parent(x) == parent(y)
    return parent(x)(x.first * y.first, x.second * y.second)
end

function AbstractAlgebra.addeq!(x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    @assert parent(x) == parent(y)
    x.first += y.first
    x.second += y.second
    return x
end

function AbstractAlgebra.add!(_::ProdRingElem{V, W}, x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    @assert parent(x) == parent(y)
    return x+y
end

function AbstractAlgebra.mul!(_::ProdRingElem{V, W}, x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    @assert parent(x) == parent(y)
    return x*y
end


AbstractAlgebra.isunit(x::ProdRingElem) = isunit(x.first) && isunit(x.second)

Base.inv(x::ProdRingElem) = parent(x)(inv(x.first), inv(x.second))

Base.:(^)(x::ProdRingElem, n::Integer) = parent(x)(x.first^n, x.second^n)


################################################################################
# Euclidean ring structure
################################################################################

function Base.divrem(x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    @assert parent(x) == parent(y)
    
    q1, r1 = divrem(x.first, y.first)
    q2, r2 = divrem(x.second, y.second)

    q = parent(x)(q1, q2)
    r = parent(x)(r1, r2)
    return q, r
end

# From divrem, we get all the other division functions
function Base.mod(x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    @assert parent(x) == parent(y)
    _, r = divrem(x, y)
    return r
end

function Base.div(x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    @assert parent(x) == parent(y)
    q, _ = divrem(x, y)
    return q
end

function Base.gcd(x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    @assert parent(x) == parent(y)
    return parent(x)(gcd(x.first, y.first), gcd(x.second, y.second))
end

function AbstractAlgebra.divides(x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    return iszero(mod(x, y))
end

function AbstractAlgebra.divexact(x::ProdRingElem{V, W}, y::ProdRingElem{V, W}) where {V <: RingElem, W <: RingElem}
    if iszero(y)
        throw(DivideError())
    end
    q, r = divrem(x, y)
    if !iszero(r)
        throw(DivideError())
    end
    return q
end

function AbstractAlgebra.canonical_unit(x::ProdRingElem)
    return parent(x)(canonical_unit(x.first), canonical_unit(x.second))
end


## Implement enough functionality to make the following work:
R = product_ring(ZZ, QQ)
a = R(ZZ(2), QQ(1, 2))
M = matrix(R, 2, 2, [1, a, 0, 1])
M * M
Rx, x = R["x"]
f = (a * x)^2

## Assuming that both rings are Euclidean and support the `divrem` function, implement `divrem` also for product rings. Try to make the following work:
hnf(M)
