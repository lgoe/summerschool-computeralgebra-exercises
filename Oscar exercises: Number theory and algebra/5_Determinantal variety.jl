using Oscar

## 1. Define the polynomial ring `R = FF_4[x_ij | 1 ≤ i ≤ 3, 1 ≤ j ≤ 3]`.
F4, a = FiniteField(2, 2)
R, x = PolynomialRing(F4, ["x_$i$j" for i in 1:3 for j in 1:3])

## 2. Define the matrix `M`.
M = matrix(R, transpose(reshape(x, 3, 3)))

## 3. Determine the defining ideal of the determinantal variety `V` of size `3x3` and rank 1, which is defined as the vanishing set of the 2-minors of `M`.
J = minors(M, 2)
I = ideal(J)

## 4. What is the dimension of V?
dim(I)

## 5. Determine `|V(FF_4)|`, where `V(FF_4) ⊆ FF_4^9`.

collect(Iterators.filter(p -> all(j -> evaluate(j, p) == 0, J), AbstractAlgebra.ProductIterator([F4 for _ in 1:9])))
count(p -> all(j -> evaluate(j, p) == 0, J), AbstractAlgebra.ProductIterator([F4 for _ in 1:9]))
