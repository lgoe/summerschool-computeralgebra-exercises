using Oscar

## 1. Define the polynomial ring `R = QQ[x,y,z]`.
R, (x,y,z) = PolynomialRing(QQ, ["x","y","z"])

## 2. Define the ideal `I = ⟨xy+z,yz-x,zx-y⟩`.
I = ideal([x*y+z, y*z-x, z*x-y])

## 3. Determine `dim(I)`.
dim(I)

## 4. Compute a Gröbner basis of `I` with respect to the lexicographical ordering.
groebner_basis(I, ordering=:lex)

## 5. Is `I` a prime ideal?
isprime(I)

## 6. Find a prime ideal containing `I`.
J = minimal_primes(I)[1]
issubset(I,J)
isprime(J)
