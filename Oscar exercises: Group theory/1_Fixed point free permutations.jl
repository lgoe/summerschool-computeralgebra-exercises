using Oscar

## 1. Write a function `f` which computes for small values of `n`, the number of fixed point free permutations on `n` points. For example, `f(1)=0`, `f(2)=1`, `f(3)=2`, `f(4)=9` and `f(30)=97581073836835777732377428235481`.
function f(n::Int)
    g = symmetric_group(n)
    conj_classes = conjugacy_classes(g)
    fp_free_classes = filter(c -> number_moved_points(representative(c)) == n, conj_classes)
    return sum(ZZ ∘ length, fp_free_classes, init=ZZ(0))
end

@show f(1) == ZZ(0)
@show f(2) == ZZ(1)
@show f(3) == ZZ(2)
@show f(4) == ZZ(9)
@show f(30) == ZZ(97581073836835777732377428235481)


## 2. Write a function which computes the proportion `x(n)` of these permutations in the symmetric group on n points, and the first decimal digits of `x(n)` and `1/x(n)`.
g(n::Int) = f(n) // factorial(n)

for i in 2:15
    @show Float64(g(i))
end
for i in 2:15
    @show Float64(1//g(i))
end

## 3. Do you have a conjecture what `lim_n→∞ x(n)` is?
for i in 2:15
    @show Float64(g(i)) - exp(-1)
end
